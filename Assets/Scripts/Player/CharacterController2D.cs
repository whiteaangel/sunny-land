using UnityEngine;
using UnityEngine.Events;

namespace Player
{
	public class CharacterController2D : MonoBehaviour
	{
		[SerializeField] private float jumpForce = 400f;							
		[Range(0, 1)] [SerializeField] private float crouchSpeed = .36f;			
		[Range(0, .3f)] [SerializeField] private float movementSmoothing = .05f;
		[SerializeField] private bool airControl;						
		[SerializeField] private LayerMask whatIsGround;							
		[SerializeField] private Transform groundCheck;							
		[SerializeField] private Transform ceilingCheck;							
		[SerializeField] private Collider2D crouchDisableCollider;

		private const float KGroundedRadius = .2f;
		private bool mGrounded;
		private const float KCeilingRadius = .2f; 
		private Rigidbody2D mRigidbody2D;
		private bool mFacingRight = true; 
		private Vector3 mVelocity = Vector3.zero;
    
		[Header("Events")]
		[Space]
   
		public UnityEvent onLandEvent;
   
		[System.Serializable]
		public class BoolEvent : UnityEvent<bool> { }
   
		public BoolEvent onCrouchEvent;
		private bool mWasCrouching;
   
		private void Awake()
		{
			mRigidbody2D = GetComponent<Rigidbody2D>();
   
			onLandEvent ??= new UnityEvent();
			onCrouchEvent ??= new BoolEvent();
		}
   
		private void FixedUpdate()
		{
			bool wasGrounded = mGrounded;
			mGrounded = false;
        
			Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, KGroundedRadius, whatIsGround);
			foreach (var t in colliders)
			{
				if (t.gameObject == gameObject) continue;
				mGrounded = true;
				
#if UNITY_EDITOR
				if (!wasGrounded)
					onLandEvent.Invoke();
#elif UNITY_STANDALONE_WIN
    if (wasGrounded)
		onLandEvent.Invoke();
#endif
			}
		}

		public void Move(float move, bool crouch, bool jump)
		{
			if (!crouch)
			{
				if (Physics2D.OverlapCircle(ceilingCheck.position, KCeilingRadius, whatIsGround))
				{
					crouch = true;
				}
			}
	    
			if (mGrounded || airControl)
			{
				if (crouch)
				{
					if (!mWasCrouching)
					{
						mWasCrouching = true;
						onCrouchEvent.Invoke(true);
					}
                
					move *= crouchSpeed;
                
					if (crouchDisableCollider != null)
						crouchDisableCollider.enabled = false;
				} else
				{
					if (crouchDisableCollider != null)
						crouchDisableCollider.enabled = true;
   
					if (mWasCrouching)
					{
						mWasCrouching = false;
						onCrouchEvent.Invoke(false);
					}
				}
	        
				Vector3 targetVelocity = new Vector2(move * 10f, mRigidbody2D.velocity.y);
				mRigidbody2D.velocity = Vector3.SmoothDamp(mRigidbody2D.velocity, targetVelocity, ref mVelocity, movementSmoothing);
            
				if (move > 0 && !mFacingRight)
				{
					Flip();
				}
				else if (move < 0 && mFacingRight)
				{
					Flip();
				}
			}
        
			if (!mGrounded || !jump) return;
			mGrounded = false;
			mRigidbody2D.AddForce(new Vector2(0f, jumpForce));
		}
   
   
		private void Flip()
		{
			mFacingRight = !mFacingRight;
	    
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
	}
}
