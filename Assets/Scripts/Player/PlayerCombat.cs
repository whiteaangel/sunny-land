using Enemy;
using UnityEngine;

namespace Player
{
    public class PlayerCombat : MonoBehaviour
    {
        [SerializeField] private PlayerMovement movement;
        [SerializeField] private GameObject raycastPoint;
        [SerializeField] private AudioSource audioSource;

        private float nextAttackTime;
        private const float Radius = 0.1f;
        private void Update()
        {
            if(movement.playerState != PlayerMovement.PlayerState.Jump)
                return;
            
            if (!(Time.time > nextAttackTime)) return;
            var hit = Physics2D.CircleCast(raycastPoint.transform.position, Radius, -Vector2.up);

            if (!hit.collider.CompareTag("Enemy") ) 
                return;
            
            var distance = Mathf.Abs(hit.point.y - transform.position.y);
            if (!(distance <= 1)) return;
            var clip = Resources.Load("Sounds/hurt", typeof(AudioClip)) as AudioClip;
            audioSource.PlayOneShot(clip);
            
            hit.collider.gameObject.GetComponent<IEnemy>().TakeDamage();
            GetComponent<Rigidbody2D>().velocity = new Vector2(0,8f);
            nextAttackTime = Time.time + 1f;
        }
    }
}
