using System;
using System.Collections;
using UnityEngine;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        public AudioSource audioSource;
        public CharacterController2D controller2D;
        public Animator animator;
        public GameObject playerSprite;
        public float runSpeed = 40f;
        public GameObject point;
        
        protected internal float health = 3f;

        public LayerMask platformMask;
        private float horizontalMove;
        private bool jump;
        private bool crouch;
        private static readonly int Speed = Animator.StringToHash("Speed");
        private static readonly int IsJumping = Animator.StringToHash("isJumping");
        
        public event Action AcornUpdate;
        protected internal int acornCount;
        private float nextPickupTime;
        private const float MAXAcornValue = 32f;

        public event Action HealthUpdate;
        public event Action WinGame;
        public event Action LooseGame;
    
        protected internal enum PlayerState
        {
            Idle,
            Jump,
            Crouch
        }

        protected internal PlayerState playerState;
        private static readonly int IsCrouching = Animator.StringToHash("isCrouching");
        private static readonly int Hurt = Animator.StringToHash("hurt");

        public void Start()
        {
            playerState = PlayerState.Idle;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ExitGame();
            }
            
            if (Input.GetButtonDown("Crouch"))
            {
                crouch = true;
                playerState = PlayerState.Crouch;
            }
            else if (Input.GetButtonUp("Crouch"))
            {
                crouch = false;
                playerState = PlayerState.Idle;
            }

            if (playerState == PlayerState.Crouch)
            {
                if (!Input.GetButtonDown("Jump")) return;
                var collider2d = Physics2D.OverlapCircle(point.transform.position, 0.2f, platformMask);
                if (collider2d == null)
                    return;
                StartCoroutine(JumpOff(collider2d));
                return;
            }
            
            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

            animator.SetFloat(Speed, Mathf.Abs(horizontalMove));
            if (!Input.GetButtonDown("Jump")) return;
            jump = true;
            playerState = PlayerState.Jump;
            animator.SetBool(IsJumping,true);
            var clip = Resources.Load("Sounds/jump", typeof(AudioClip)) as AudioClip;
            audioSource.PlayOneShot(clip);
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(point.transform.position,0.2f);
        }

        private static IEnumerator JumpOff(Component collider2d)
        {
            collider2d.GetComponent<PlatformEffector2D>().rotationalOffset = 180;
            Physics2D.IgnoreLayerCollision(3,7,true);
            yield return new WaitForSeconds(0.3f);
            Physics2D.IgnoreLayerCollision(3,7,false);
            collider2d.GetComponent<PlatformEffector2D>().rotationalOffset = 0;
        }
    
        public void OnLanding()
        {
            if(playerState != PlayerState.Jump)
                return;
            animator.SetBool(IsJumping,false);
            playerState = PlayerState.Idle;
        }

        public void OnCrouching(bool isCrouching)
        {
            animator.SetBool(IsCrouching, isCrouching);
        }

        public void TakeDamage(int damage)
        {
            if(health <= 0)
                return;
            
            var clip = Resources.Load("Sounds/hurt", typeof(AudioClip)) as AudioClip;
            audioSource.PlayOneShot(clip);
            
            health -= damage;
            animator.SetTrigger(Hurt);
            
            HealthUpdate?.Invoke();

            if (!(health <= 0)) return;
            LooseGame?.Invoke();
            StartCoroutine(DestroyPlayer());
            playerSprite.SetActive(false);
        }

        private IEnumerator DestroyPlayer()
        {
            yield return new WaitForSeconds(0.2f);
                Destroy(gameObject);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Acorn")) return;
            if (!(Time.time > nextPickupTime)) return;
            acornCount++;
            var clip = Resources.Load("Sounds/pickup", typeof(AudioClip)) as AudioClip;
            audioSource.PlayOneShot(clip);
            AcornUpdate?.Invoke();
            Destroy(other.gameObject);

            if (acornCount >= MAXAcornValue)
            {
                WinGame?.Invoke();
            }
            nextPickupTime = Time.time + 0.1f;
        }
        
        private void FixedUpdate()
        {
            controller2D.Move(horizontalMove * Time.fixedDeltaTime,crouch,jump);
            jump = false;
        }

        private void ExitGame()
        {
            Application.Quit();
        }
    }
}
