using Player;
using UnityEngine;

public class DeathCollider : MonoBehaviour
{
   private void OnTriggerEnter2D(Collider2D other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
        other.GetComponent<PlayerMovement>().TakeDamage(3);
      }
   }
}
