using Player;
using UnityEngine;

namespace Enemy
{
    public class GatorEnemy : MonoBehaviour, IEnemy
    {
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] [Range(0,1)] private float speed = 1f;
        [SerializeField] [Range(0,100)] private float range = 1f;
    
        private float startPos;

        private const float Radius = 0.5f;
        private float nextAttackTime;

        private void Start()
        {
            startPos = transform.position.y;
        }

        private void Update()
        {
            var yPos = Mathf.PingPong(Time.time * speed, 1) * range;
            transform.position = new Vector3(transform.position.x, startPos + yPos, transform.position.z);
        
            Attack();
        }

        private void Attack()
        {
            if (!(Time.time > nextAttackTime)) return;
            var collider2d = Physics2D.OverlapCircle(transform.position, Radius, playerLayer);
            if(collider2d == null)
                return;
            collider2d.GetComponent<PlayerMovement>().TakeDamage(1);
            nextAttackTime = Time.time + 1f;
        }
    
        public void TakeDamage()
        {
            Instantiate(Resources.Load("Death"),transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
