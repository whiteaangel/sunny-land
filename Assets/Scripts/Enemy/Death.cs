using System.Collections;
using UnityEngine;

namespace Enemy
{
    public class Death : MonoBehaviour
    {
        private void Awake()
        {
            StartCoroutine(Destroy());
        }

        private IEnumerator Destroy()
        {
            yield return new WaitForSeconds(0.6f);
            Destroy(gameObject);
        }
    }
}
