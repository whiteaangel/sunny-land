namespace Enemy
{
    public interface IEnemy
    {
        public void TakeDamage();
    }
}
