using Player;
using UnityEngine;

namespace Enemy
{
    public class AntEnemy : MonoBehaviour,IEnemy
    {
        [SerializeField] private Transform attackPoint;
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private float maxDist = 5f;
        [SerializeField] private float minDist = 3f;
        [SerializeField] private int direction;
        private const float Radius = 0.5f;
        private float nextAttackTime;
        
        private Rigidbody2D mRigidbody2D;
        
        private const float MovingSpeed = 5f;
        private const float MMovementSmoothing = .05f;
        private Vector3 mVelocity = Vector3.zero;

        private void Start ()
        {
            mRigidbody2D = GetComponent<Rigidbody2D>();
        }
        
        private void Update()
        {
            switch (direction)
            {
                case -1:
                    if( transform.position.x > minDist)
                        Move();
                    else
                    {
                        direction = 1;
                        transform.Rotate(0f, 180f, 0f);
                    }

                    break;
                case 1:
                    if(transform.position.x < maxDist)
                        Move();
                    else
                    {
                        direction = -1;
                        transform.Rotate(0f,180f,0f);
                    }

                    break;
            }
            
            Attack();
        }
        
        private void Move()
        {
            var horizontalMove = direction * MovingSpeed;
            var velocity = mRigidbody2D.velocity;
            Vector3 targetVelocity = new Vector2( Time.fixedDeltaTime * horizontalMove * 10f, velocity.y);
            mRigidbody2D.velocity = Vector3.SmoothDamp(velocity, targetVelocity,
                ref mVelocity, MMovementSmoothing);
        }

        private void Attack()
        {
            if (!(Time.time > nextAttackTime)) return;
            var collider2d = Physics2D.OverlapCircle(attackPoint.position, Radius, playerLayer);
            if(collider2d == null)
                return;
            collider2d.GetComponent<PlayerMovement>().TakeDamage(1);
            nextAttackTime = Time.time + 1f;
        }
  
        public void TakeDamage()
        {
            Instantiate(Resources.Load("Death"),transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
