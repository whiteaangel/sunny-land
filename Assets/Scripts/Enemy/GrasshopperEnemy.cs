using System.Collections;
using Player;
using UnityEngine;

namespace Enemy
{
    public class GrasshopperEnemy : MonoBehaviour, IEnemy
    {
        [SerializeField] private Transform jumpPoint;
        [SerializeField] private Transform attackPoint;
        [SerializeField] private Transform groundPoint;
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private Animator animator;
        
        private const float Radius = 0.5f;
        private float nextAttackTime;
        private float nextJumpTime;

        private bool isFindPlayer;
        private bool isJumping;
        private Rigidbody2D rb;
        private static readonly int Jump = Animator.StringToHash("Jump");

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }
        
        private void Update()
        {
            if (!isFindPlayer)
            {
                FindPlayer();
                return;
            }

            if (!isJumping)
            {
                if (!(Time.time > nextJumpTime)) return;
                rb.AddForce(new Vector2(0, 5000f));
                animator.SetTrigger(Jump);
                isJumping = true;
                nextJumpTime = Time.time + 1f;
            }
            
            Attack();
            StartCoroutine(OnGround());
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(jumpPoint.position, 0.2f);
            
        }

        private void FindPlayer()
        {
            var collider2d = Physics2D.OverlapCircle(jumpPoint.position, 0.2f, playerLayer);
            if(collider2d == null)
                return;
            isFindPlayer = true;
        }
        
        private void Attack()
        {
            if (!(Time.time > nextAttackTime)) return;
            var collider2d = Physics2D.OverlapCircle(attackPoint.position, Radius, playerLayer);
            if(collider2d == null)
                return;
            collider2d.GetComponent<PlayerMovement>().TakeDamage(1);
            nextAttackTime = Time.time + 2f;
        }

        private IEnumerator OnGround()
        {
            yield return new WaitForSeconds(1f);
            var collider2d = Physics2D.OverlapCircle(groundPoint.position, 0.5f, groundLayer);
            if(collider2d == null)
               yield break;
            isFindPlayer = false;
            isJumping = false;
        }

        public void TakeDamage()
        {
            Instantiate(Resources.Load("Death"),transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
