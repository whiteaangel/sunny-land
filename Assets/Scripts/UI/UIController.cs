using System;
using System.Globalization;
using Player;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private PlayerMovement playerMovement;
    [SerializeField] private Text acornCountText;
    [SerializeField] private Text healthCountText;
    [SerializeField] private GameObject startPanel;
    [SerializeField] private GameObject winGamePanel;
    [SerializeField] private GameObject looseGamePanel;
    
    private void OnEnable()
    {
        playerMovement.AcornUpdate += AcornUpdate;
        playerMovement.HealthUpdate += HealthUpdate;
        playerMovement.WinGame += WinGame;
        playerMovement.LooseGame += LooseGame;
    }

    private void OnDisable()
    {
        playerMovement.AcornUpdate -= AcornUpdate;
        playerMovement.HealthUpdate -= HealthUpdate;
        playerMovement.WinGame -= WinGame;
        playerMovement.LooseGame -= LooseGame;
    }

    public void Awake()
    {
        HealthUpdate();
        Time.timeScale = 0f;
    }
    
    public void StartGame()
    {
        Time.timeScale = 1f;
        startPanel.SetActive(false);
    }

    private void WinGame()
    {
        Time.timeScale = 0f;
        winGamePanel.SetActive(true);

    }

    private void LooseGame()
    {
        Time.timeScale = 0f;
        looseGamePanel.SetActive(true);
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void AcornUpdate()
    {
        acornCountText.text = playerMovement.acornCount.ToString();
    }

    private void HealthUpdate()
    {
        healthCountText.text = playerMovement.health > 0 ? playerMovement.health.ToString(CultureInfo.InvariantCulture) : "0";
    }
}
